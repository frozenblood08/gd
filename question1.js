/*
  Author: Karan Tuteja
  github: https://github.com/frozenblood07
*/


/*
1. Write a program that will print three elements in an array whose sum is equal to a given number.
*/

var inputArr = [5,10,8,100,22,11,1,9],
sum = 31;

//This function sorts integer array in asc order
function sortArrAsc(numArray){
	numArray.sort(function (a, b) {  return a - b;  });
	return numArray
}



//This function will find the check the input array and if present print the 3 elements whose sum is equal to the input sum given else print not found
function findTriplets(inputArr,sum){
	
	//sort the input array in asc order
	var sortedArr = sortArrAsc(inputArr),
	leftCounter,
	rightCounter;

	//fix the first element one by one and find the other two elements 
	for(i=0;i<sortedArr.length - 2;i++)  {
 
        //find the other two elements, start two index variables from two corners and move them
        leftCounter = i + 1; //index of the first element in the remaining array
        rightCounter = sortedArr.length-1; //index of the last element

        while (leftCounter < rightCounter)
        {
            if( sortedArr[i] + sortedArr[leftCounter] + sortedArr[rightCounter] == sum)
            {
            	//triplets found
                console.log("Triplet is", sortedArr[i], sortedArr[leftCounter], sortedArr[rightCounter]);
                return true;
            }
            else if (sortedArr[i] + sortedArr[leftCounter] + sortedArr[rightCounter] < sum){
                leftCounter++;
            }
            else{
                rightCounter--;
            }
        }
    }
 
    //no triplets was found
    console.log("No triplets found");
    return false;
}


findTriplets(inputArr,sum);