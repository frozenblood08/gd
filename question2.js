/*
  Author: Karan Tuteja
  github: https://github.com/frozenblood07
*/

/*
2. I have some friends records in a text file (say, friends.json) -- one friend per line, JSON-encoded. 
I want to invite any friends within 100km of our New Delhi office (GPS coordinates 28.521134, 77.206567) for some food and drinks on us. 
Write a program that will read the full list of friends and output the names and user ids of matching friends (within 100km), sorted by user id (ascending).
*/


var fileSystem = require('fs');


var fromDistanceLatitude = 28.521134,
toDistanceLongitude= 77.206567,
maxDistance = 100;

//this function read a file synchronously
function readFile(path){
	
	var contents;
	
	try{
		contents = fileSystem.readFileSync(path, 'utf8');
		return contents;
	}
	catch(err){
		if (err.code === 'ENOENT') {
		  console.log('File not found!');
		} else {
		  throw err;
		}
	}
}

//this function will convert from  a value degree to radian
function deg2rad(deg) {
  return deg * (Math.PI/180)
}

//this function will return distance between 2 lat lon in kms
function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}

//this function will sort the array of objects in ascending order based on the ids of object
function sortArrObjAsc(arrInput){
	arrInput.sort(function(a, b) {
    	return a.id - b.id;
	});

	return arrInput;
}

//this function will print a list of all the close friends in the given list who are closer than the maxdistance provided to the function
function getCloseFriendsInfo(maxDistance){
	var friendsInfo = readFile('./friends.json'),
	closeFriends = [],
	distance;

	friendsInfo = JSON.parse(friendsInfo); //converting to object

	//getting the distance of each friend from the point
	for(i=0;i<friendsInfo.length;i++){
		
		distance = getDistanceFromLatLonInKm(fromDistanceLatitude,toDistanceLongitude,friendsInfo[i].latitude,friendsInfo[i].longitude);

		if(distance < maxDistance){
			closeFriends.push(friendsInfo[i]);
		}
	}

	//sorting the required friends by id
	closeFriends = sortArrObjAsc(closeFriends);

	console.log(closeFriends);
}

getCloseFriendsInfo(maxDistance);


